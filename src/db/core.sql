-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 26, 2022 at 04:31 PM
-- Server version: 5.7.34
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `core`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(8) UNSIGNED NOT NULL,
  `text_message` varchar(256) DEFAULT NULL,
  `timestamp` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(8) UNSIGNED NOT NULL,
  `email` varchar(63) DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `email`, `timestamp`) VALUES
(1, 'daniel.kyndl@seznam.cz', '2022-01-25 19:30:41'),
(3, 'waksj@ksjdaa.cu', '2022-01-25 19:32:12'),
(6, 'ahoj@jaksemas.cz', '2022-01-25 19:41:17'),
(8, 'jaksemas@ahoj.com', '2022-01-25 19:43:10'),
(9, 'neak@ask.cz', '2022-01-25 19:44:10'),
(10, 'lask@as.ca', '2022-01-25 19:44:29'),
(11, 'green@errorbar.com', '2022-01-25 19:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(8) UNSIGNED NOT NULL,
  `email` varchar(63) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `login` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `login`) VALUES
(1, 'jan.novak@email.cz', '$2y$10$E8sDkTJrbkZPtQninEAfYuV.dua33QbHFyMsjg1Dsyo3lPNPbILJW', '23.01.2022'),
(2, 'kyndl@animato.cz', '$2y$10$meO.IWB1P1zeJ4VdfJhaduAUFcgDlTxMc0dvBk5bQY9XXIHNVi142', '19.01.2022'),
(3, 'ahoj@email.cz', '$2y$10$PYgBTaNDaIJoSxLxYagPUOQCPn0nD38Xl6Ok.Z9aWYTT7abb.jz6O', '16.01.2022'),
(15, 'example@example.org', '$2y$10$3E1SC7ldwaJfMfMt0o924ukRPzB7iOf.ET77yVKgbDCxYmyjK5m5C', 'never');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
