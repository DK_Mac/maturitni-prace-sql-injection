// Centrální spouštěcí stránka, kpřes kterou běží včechny skripty přiložené v rámci components.

import {

    nav, activate

} from './components/nav.js';

import {

    changeColor, getChapters, responsive

} from './components/navbar.js';
import {

    stats

} from './components/chart.js';
import {

    contentCarousel

} from './components/content-carousel.js';
import {

    errors

} from './components/errors.js';


getChapters();
nav();
changeColor();
responsive();
stats();
contentCarousel();
errors();
activate();
