/*

Funkce, která pracuje s errorovými hláškami, které generuje E_2_lines(), což je fce generovaná v popup_functions.php
Vlastně zase jen řídí css animace

*/

export function errors() {

    let errors = document.getElementsByClassName("e-error--js_target");
    for(let x of errors) {

        x.addEventListener("click", () => {

            hide_element(x);

        });

        setTimeout(() => {

            hide_element(x)

        }, 8000)

    }

}

function hide_element(x) {

    x.style.transform = "translateX(100%)";
    setTimeout(() => {

        x.style.display = "none";

    }, 150)

}