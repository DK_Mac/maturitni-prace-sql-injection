export function stats() {

    // Funguje díky importovanému modulu chart.js, přiloženého pomocí cdn. Tohle je de facto jen config.
    // asi není potřeba to moc komentovat, ono to je hodně primitivní a je to de facto jen malinko poupravený příklad,
    // Co mají sami v dokumentaci

    const ctx = document.getElementById('Stats');

    if(ctx != null) {

        ctx.getContext('2d');

        const stats = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Dnes', 'Včera', 'Předvčírem', 'Den před tím', 'A tak', 'dále'],
                datasets: [{
                    label: '# návštěv',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    

    }

}
