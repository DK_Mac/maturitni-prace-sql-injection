export function changeColor() {

    window.onscroll = () => {

        let nav = document.getElementsByClassName("e-navbar")[0];
        let offset = 60;

        if (document.querySelector(".o-article__name")) {

            offset = 172;  // Tohle chci u článků, kde je modrý nadpis

        }

        // Event listenery na child elementy, aby se zobrazovala active třída

        if(nav) {

            if (document.body.scrollTop > offset || document.documentElement.scrollTop > offset) {
                // Aneb kde zmodrá navbar
                nav.classList.add("e-navbar--scrolled");

            }
            else {

                nav.classList.remove("e-navbar--scrolled");

            }

        }

    }

}

// Změna barvičky navigace, když kousek srolluju dolů..




export function responsive() {

    let navbar = document.querySelector(".e-navbar");

    if (!navbar) {
        return // Když to není na stránce, přeruším fci
    }

    let elements = Array.from(navbar.children);  // Potřebuji si vstup předělat na normální array, tohle je set DOM nód
    let head = elements.shift();  // Vyhazuji nultý člen, tedy mobilní line. Té se to netýká.

    let button = document.querySelector(".e-navbar__mobile_control");
    button.addEventListener("click", () => {

        // Tohle je vlastně popis animace, která se stane, když user klikne na tlačítko

        let c_active = "e-navbar__button--active";

        if(button.classList.contains(c_active)) {
            
            button.classList.remove(c_active);
            head.style.borderBottom = "1px solid transparent";

            for(let x of elements) {

                x.style.opacity = 0;

                setTimeout(() => {
                    x.classList.remove("e-navbar__link--active");
                    navbar.style.height = "70px";
                }, 150);

            }

        }
        else {
            button.classList.add(c_active);
            head.style.borderBottom = "1px solid grey";
            navbar.style.height = (70 + (elements.length -1) * 36 + 45) + "px";

            for(let x of elements) {

                x.classList.add("e-navbar__link--active");

                setTimeout(() => {
                    x.style.opacity = 1;
                }, 150);

            }
        }


    });

}


export function getChapters() {

    // Automaticky přidává kapitoly do navbar komponenty.
    // Useful věc pro blogy, vytváří to v podstatě .md prostředí...

    let h1;
    let dest = document.querySelector(".e-navbar--article");

    if (document.querySelector(".l-section")) {
        h1 = document.querySelectorAll(".c-headline");
    }

    else {
        h1 = document.querySelectorAll(".o-article__headline");
    }

    if (dest) {

        let text, id;
        let anchors = document.querySelectorAll(".anchor");
        for (let i = 0; i < h1.length; i++) {

            text = h1[i].textContent;
            id = anchors[i].id;

            text = text.replace("\n            ", "");
            text = text.replace("\n        ", "");

            let a = document.createElement("a")
            a.className = "e-navbar__link";
            a.innerHTML = text;
            a.href = "#" + id

            dest.appendChild(a)

        }

    }

}
