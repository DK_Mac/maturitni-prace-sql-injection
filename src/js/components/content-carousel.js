export function contentCarousel() {

    let parent = document.querySelector(".js-content-carousel");

    for (let x of document.getElementsByClassName("js-content-carousel-btn")) {

        x.addEventListener("click", function() {

            if(parent.classList.contains("js-active")) {

                parent.style.left = 0;
                parent.classList.remove("js-active");

            }

            else {

                parent.style.left = "-100%";
                parent.classList.add("js-active");

            }

        })

    }

}

// Slouží ke změně pohledů na přidávací stránku u csv souborů.
// Pomocí metody addEventListener.