export function nav() {

    /**
     * Tahle funkce je poměrně podstatná pro fungování side navu.
     * Sice to vypadá složitě, ale v podstatě to je jednoduché
     * Dějí se dvě věci zároveň:
        1. vyrolování / zarolování navigace
        2. Posun a změna textu tlačítka
     * Takže zase jen funkce, která řídí dvě animace
     * Takhle komplexní to je jenom z důvodu časování
     */

    for (let w of (document.getElementsByClassName("js-nav"))) {
        w.addEventListener("click", function() {
            let nav = document.querySelector(".e-nav");

            let links = document.getElementsByClassName("e-nav__link");

            if (!(this.classList.contains("js-nav--active"))) {

                this.classList.add("js-nav--active");
                this.innerHTML = "<"

                nav.style.display = "block";

                setTimeout(() => {

                    nav.classList.remove("e-nav--asleep");

                }, 10)

                setTimeout(() => {

                    for (let x of links) {

                        x.style.opacity = 1;

                    }

                }, 300)

            }

            else {

                for (let x of links) {

                    x.style.opacity = 0;

                }

                let button = this;

                setTimeout(() => {

                    button.classList.remove("js-nav--active");
                    button.innerHTML = ">"

                    nav.classList.add("e-nav--asleep");

                    setTimeout(() => {

                        nav.style.display = "none";

                    }, 200)

                }, 100)

            }

        });

    }
}



export function activate() {

    var url = window.location.href.split("#")[0];
	var links = document.getElementsByClassName("e-nav__link");

	for(let x of links)   {

		if(x.href == url) {

            x.classList.add("e-nav__link--active");
			break;

		}

	}

}
// Tahle funkce sama zjistí, na které stránce se nacházím a podle toho přidá třídu active dotyčnému linku v navigaci
