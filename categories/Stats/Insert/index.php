<?php
session_start();

require $_SERVER['DOCUMENT_ROOT']. '/core/mod/popup_functions.php';

// Jednoduchý systém na zobrazování error hlášek (pro přiložení souboru)

if (isset($_SESSION['curr-action']) && $_SESSION['curr-action'] == "") {

	switch($_SESSION['curr-action']) {
		case 'SUCCESS':
			E_2_lines("Akce proběhla úspěšně", "Data byla úspěšně přidána", "success");
			break;

		case 'FAIL-1':
			E_2_lines("Při zpracování žádosti nastala chyba.", "Zřejmě byl zvolen špatný soubor. Zkuste to prosím znovu", "danger");
			break;

		case 'FAIL-2':
			E_2_lines("Nastala neočekávaná a poměrně dost zajímavá chyba", "Abychom se přiznali, nevíme, co se pokazilo, ale bylo to na serveru.", "danger");
			break;

		default:
			break;
	}

}

// Mimochodem, na týhle stránce je docela vtipný, když se stane nějaká PHP chyba... :))
// Objeví se totiž v pozadí za těma dvěma obdélníkama a je vidět jenom když se člověk přesouvá mezi nimi. :O
// V rámci obhajoby můžu pro pobavení demonstrovat, stačí přidat nahoře vykřičník... :))

?>

<!DOCTYPE html>
<html lang="cs">
<head>
	<?php
	require $_SERVER['DOCUMENT_ROOT'] . '/src/html/header.html';
	?>
</head>
<body class="l-body l-body--center">
<!-- Co se týče HTML, je to rozdělené na dvě podstránky ukotvené vedle sebe a jen se posouvá pohled. -->
<!-- 
Obecně je tenhle insert systém a pak i systém čtení dost primitivní. Byla to jen část menší práce na matiku
A já se to rozhodnul si hrát... No, to je tak, když člověk má před Vánocema málo co na práci,
nemá sport, nemá přítelkyni a sociální život obecně... :)))

Ne, ale stejně jsem to nehrotil. Mohl jsem přidat podporu psaní čísel do řádky (což by asi dávalo smysl),
stejně jako další věci, ale to už se mi zase nechtělo.
Pokud se ještě na poslední chvíli nerozhodnu tady nějaký input zneužít a tenhle komentář neodmažu, 
tak by tohle mělo být pro úkol maturitní práce úplně nepodstatné.

Předem se omlouvám všem, kteří sem došlí a musí tohle číst. Ano, asi jsem se nechal trochu unést :) Ale mě to baví
-->


<div class="l-2-grid js-content-carousel">
	<div class="l-100-width l-100-width--center">
		<div class="o-login">
			<div class="o-login__talkie">
				<h2 class="o-login__headline">
					Vložit CSV soubor
				</h2>
				<p class="o-login__text">
					Tady není potřeba hledat chybu.
				</p>
				<hr>
				<p class="o-login__text">
					Zde lze ručně nahrát soubory se statistickými daty. S těmi lze poté pracovat
					na stránce <a class="o-login__link--white" href="../Calculate/">Calculate</a>.
				</p>
			</div>
			<div class="o-login__form-holder">

				<form class="e-form" method="post" action="/core/mod/file_upload.php" id="file-upload" enctype="multipart/form-data">

					<label class="e-form__label" for="upload">CSV by měl obsahovat pouze 1 sloupec s daty</label>
					<input class="e-form__input" type="file" name="upload" id="upload" autocomplete="false">
					<input type="hidden" name="type" value="CSV" id="type">
					<input type="hidden" name="origin" value="Stats/Insert">

					<button type="submit" class="c-button c-button--submit">
						Vložit
					</button>

				</form>

			</div>

			<button class="c-button c-button--success c-button--mb c-button---mt js-content-carousel-btn">
				Nahrát data ručně >>
			</button>

		</div>
	</div>

	<!-- Tady začíná druhá "stránka" -->

	<div class="l-100-width l-100-width--center">
		<div class="o-login">
			<div class="o-login__talkie">
				<h2 class="o-login__headline">
					Ručně přidat data
				</h2>
				<p class="o-login__text">
					Slouží pro ruční nahrátí dat pro vytvoření statistiky.
				</p>
				<hr>
				<p class="o-login__text">
					Data jsou uložena jako CSV Soubor (podle pojmenování) a jsou pak dále přístupná
					na stránce <a class="o-login__link--white" href="../Calculate/">Calculate</a>.
				</p>
			</div>
			<div class="o-login__form-holder">

				<form class="e-form" method="post" action="/core/mod/file_create.php" id="file-create" enctype="multipart/form-data">

					<label class="e-form__label" for="upload" required>Název csv souboru (bez koncovky)</label>
					<input class="e-form__input" type="text" name="name" id="name" title="snehulak" required>

					<label for="data" class="e-form__label">Čísla oddělujte novým řádkem. Potáhněte v prvém dolním okraji pro úpravu</label>
					<textarea class="e-form__input e-form__input--textarea" name="data" id="data" cols="30" rows="10" required></textarea>

					<input type="hidden" name="type" id="type" value="csv">

					<button type="submit" class="c-button c-button--submit">
						Uložit
					</button>
				</form>

			</div>

			<button class="c-button c-button--success c-button--mb c-button---mt js-content-carousel-btn">
				<< Přidat .csv soubor
			</button>

		</div>
	</div>
</div>



<?php
require $_SERVER['DOCUMENT_ROOT'] . '/src/html/nav.html';
?>



<?php
require $_SERVER['DOCUMENT_ROOT'] . '/src/html/scripts.html';
?>
<script>
	$(document).ready(function() {
        $("#file-create").submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: "/core/mod/file_create.php",
                type: "post",
                data: {
					name: $("#name").val(),
                    data: $("#data").val(),
                    type: $("#type").val()
                },
                success: function(response) {
                    if (response == "OK") {
						alert("Soubor úspěšně vytvořen");
						$('#file-create').find("input[type=text], textarea").val("");
					} else {
                        alert('Chyba na straně serveru');
                    }
                }
            })
        })
    })
</script>
<!-- Jen krátký jQuery script, který vlastně odesílá data do file_create.php. Výhoda je, že nepřesměruje, tudíž nemusím řeišt nic jako headers.  -->

</body>
</html>