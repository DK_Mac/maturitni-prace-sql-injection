<?php

$values = $allArr;
array_shift($values);

function validate() {

    // Součást dalších funkcí, zkišťuje, jestli je vybraná nějaká složka na spočítání dat

    if (isset($_GET['target']) && htmlspecialchars($_GET['target']) != '') {

        return true;

    }

    else {

        return false;

    }

}

function avg($values) {

    // Spočítá průměr ze zadaných čísel. Zavolána v stats/calculate
    // Values je array, bude tedy potřeba ho téměř pokařdé procyklit a vyndat z něj hodnoty...

    if (validate()) {

    $res = 0;

    foreach($values as $value) {

        $res += (int)$value;
        //Tady to slíbené procyklení

    }

    // Vracím výsledek, který je potom vytištěný v Indexu.
    return($res/count($values));

    }

}

function harmonic_avg($values) {

// Spočte harmonický průměr importovaných čísel
// Stejný princip jako nahoře, jen jiný vzorec

    if (validate()) {

        $arithmetic = 0;
        $count = 0;

        foreach($values as $value) {

            if ((int)$value != 0) {

                $arithmetic += 1 / (int)$value;
                $count ++;

            }

            else {

                return 'Chyba, soubor obsahuje nulu! Došlo by tedy k dělení nulou...';
                break;
                exit();

            }

        }

        return 1 / ($arithmetic / $count);

    }

}

function geometric_avg($values) {

    // Počítá geometrický průměr, stejná metoda jako nahoře...

    if (validate()) {

        $res = 1;

        for ($i = 0; $i < count($values); $i++) {

            if($values[$i] < 0) {

                // Ošetření proti dělení nulou

                return 'Chyba, soubor obsahuje záporné číslo!';
                break;
                exit();

            }

            $res *= (int)$values[$i];

        }

        return pow($res, (1 / count($values)));

    }

}

function mod($values) {

    // Počítám modus, tady je to trochu jiné tím, že nemusím iterovat, stačí v něm vyhledat nejčastější hodnotu.

    if (validate()) {

        $all = array_count_values($values);
        return array_search(max($all), $all);

    }

}

function median($values) {

    // Medián, musím rozdělit na 2 větve pro sudý a pro lichý počet prvků

    if(validate()) {

        sort($values);

        if(count($values) % 2) {

            return $values[floor(count($values)/2)];

        }

        else {

            return ($values[(count($values) / 2 - 0.5)] + $values[(count($values) / 2 + 0.5)]) / 2;

        }

    }

}

function spread($values) {

    // Rozptyl, zde potřebuji pro vzorec i aritmetický průměr... Tak si ho přidám :)

    if(validate()) {

        $mean = avg($values);

        $spread_res = 0;

        foreach($values as $value) {

            $spread_res += pow(((int)$value - $mean), 2);

        }

        return 1 / count($values) * $spread_res;

    }

}

function standard_deviation($values) {

    // Standartní deviace

    if(validate()) {

        $mean = avg($values);

        $spread_res = 0;

        foreach($values as $value) {

            $spread_res += pow(((int)$value - $mean), 2);

        }

        return sqrt(1 / count($values) * $spread_res);

    }

}

function spread_const($values) {

    // Tady už se pak jen dělá statistická rozptylová konstanta.

    if(validate()) {

        $mean = avg($values);

        $spread_res = 0;

        foreach($values as $value) {

            $spread_res += pow(((int)$value - $mean), 2);

        }

        $sx = sqrt(1 / count($values) * $spread_res);

        return($sx / $mean * 100);

    }

}