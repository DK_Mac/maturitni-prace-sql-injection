<?php
session_start();

$root = $_SERVER['DOCUMENT_ROOT'];

$arrFiles = glob($root . '/core/source/files/*.csv');

$allArr = array('Ještě jste nevybrali žádná data...');

require $root . '/core/mod/fetch_file.php';
require '../Math/statistics_module.php';

/* Tady tenhle vršek, hlavička vlastně dělá 2 věci:
    1. Shání seznam všech složek, které jsou v /core/... a nastavuje fallback
    2. Přidává vlastní php soubory, které dělají magii s daty. fetch_file vybírá složku, která se otevře, statistics přidává výpočty statistik
*/

?>

<!DOCTYPE html>
<html lang="cs">

<head>
    <?php
	require $_SERVER['DOCUMENT_ROOT'] . '/src/html/header.html';
	?>

    <title>Statistická kalkulačka</title>
</head>

<body class="l-body l-body--overflow-y">

    <div class="l-flex-946"> 
        <div class="e-card">
            <div class="e-card__headline">Zvolte datový soubor</div>
            <div class="e-card__body e-card__body--fix-height">


            <!-- Forma, která odkazuje sama na sebe, Vyhodnocená data budou poslána na tuto stránku. Generováno PHP -> čitelnost a bezpečnost -->
                <form class="e-form" method="get" action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>" id="picker">
                    <div class="e-form__row">
                        <div class="e-form__column">
                            <label for="target" class="e-form__label">
                                Vyberte&nbsp;datový&nbsp;soubor:
                            </label>
                            <select name="target" id="target" class="e-form__input e-form__input--ml-only">
                                <?php
                                //For loop probere všechny možnost a vytiskne je jako <option></option>
                                for ($i = 0; $i < count($arrFiles); $i++) {
                                    $output = str_replace('/Applications/MAMP/htdocs/core/source/files/', '', $arrFiles[$i]);
                                ?>
                                    <option value="<?= $output ?>"><?= $output ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="e-form__column">
                            <button type="submit" class="c-button c-button--submit">
                                Potvrdit výběr
                            </button>
                        </div>
                    </div>
                </form>


                <?php

                    // Pokud je vybraná složka, přidám link na download
                    if($link !== false) {
                        ?>
                    <a class="e-card__link" href="/core/source/files/<?=$link?>" download>Stáhnout soubor (<?=$link?>)</a>
                        <?php
                    }

                ?>
            </div>
        </div>

        <div class="e-card">
            <div class="e-card__headline">Náhled souboru</div>
            <div class="e-card__body e-card__body--fix-height e-card__body--jc-start">
                <h4 class="c-text--centered">
                    <?php
                        echo $allArr[0];
                        array_shift($allArr);
                        // Do náhledu vyhodím název tabulky a vyrazím ho z pole s výsledky
                    ?>
                </h4>
                <p class="c-text">
                    <?php
                        // Vypíšu všechny zbývající možnosti
                        echo implode(', ', $allArr);
                    ?>
                </p>
            </div>
        </div>
    </div>

    <div class="e-card e-card--full-width">
        <div class="e-card__headline">Vypočítané statistiky</div>
        <div class="e-card__body">
            <table class="e-table e-table--striped">
                <thead>
                    <tr>
                        <th style="text-align: left;">Veličina</th>
                        <th>Hodnota</th>
                    </tr>
                </thead>
                <tbody>
                    <?//Všechny následující funkce jsou vytvořeny v souboru categories/Math/statistics_module.php//?>
                    <tr>
                        <td>Aritmetický průměr</td>
                        <td class="e-table__td--center"><?=avg($values)?></td>
                    </tr>
                    <tr>
                        <td>Harmonický průměr</td>
                        <td class="e-table__td--center"><?=harmonic_avg($values)?></td>
                    </tr>
                    <tr>
                        <td>Geometrický průměr</td>
                        <td class="e-table__td--center"><?=geometric_avg($values)?></td>
                    </tr>
                    <tr>
                        <td>Modus</td>
                        <td class="e-table__td--center"><?=mod($values)?></td>
                    </tr>
                    <tr>
                        <td>Medián</td>
                        <td class="e-table__td--center"><?=median($values)?></td>
                    </tr>
                    <tr>
                        <td>Rozptyl</td>
                        <td class="e-table__td--center"><?=spread($values)?></td>
                    </tr>
                    <tr>
                        <td>Směrodatná odchylka</td>
                        <td class="e-table__td--center"><?=standard_deviation($values)?></td>
                    </tr>
                    <tr>
                        <td>Variační koeficient (v %)</td>
                        <td class="e-table__td--center"><?=spread_const($values)?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php
    require $_SERVER['DOCUMENT_ROOT'] . '/src/html/nav.html';
    ?>

    <?php
    require $_SERVER['DOCUMENT_ROOT'] . '/src/html/scripts.html';
    ?>

</body>

</html>