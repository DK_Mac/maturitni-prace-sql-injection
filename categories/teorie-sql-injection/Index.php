<?php

$webroot = $_SERVER['DOCUMENT_ROOT'];

$title = "Teorie SQL Injection"; // Absolutní page title
require $webroot . '/core/constructors/construct_article.php';



// Co se to tady do háje děje...

// Tohle je podobná struktura, kterou používají pokročilejší PHP-based CMS systémy
// Jenže my jsme tady poměrně omezení zdroji, časem a rozsahem, takže to tady uděláme jen takové skromnější.
// Tohle php se musí vždy zkopírovat do nového adresáře, mění se jen $title.
// Soubor fetch_article dá stránku dohromady z jednotlivých komponent

// Teoreticky, kdyby tohle byl reálný server napojený na reálné CMS, tohle by se dalo zoptimalizovat použítím
    // nějakých ID generovaných z databáze, které by automaticky přiložily správný článek. Takhle to uděláme z $title.
// Reálné CMS by samo vytvořilo na serveru nový php soubor a nakopírovalo do něj tenhle obsah.
// --> Obsah stránky jako takové by se pak dal generovat přímo ze systému, zapisoval by se do souboru v src/html/articles/,
    // který by byl přiřazený danému id

// Nevýhoda tohohle systému je ta, že se tenhle soubor tedy musí nakopírovat do každé obsahové složky a jediné, co se změní, tak je webroot.
// Všechno je Čistě pro jednoduchost. A ano, tohle je nápad z mého CMS, které si dělám jen tak pro sebe...
