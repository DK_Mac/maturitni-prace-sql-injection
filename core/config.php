<?php

$servername = "localhost";
$username = "root";
$password = "root";

try {
    $db = new PDO("mysql:host=$servername;dbname=core", $username, $password);

    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch(PDOException $e) {

    echo "Connection failed: " . $e->getMessage();
    exit();

}

/***************
 * 
 * 
 * Velmi důležitá záležitost, slouží k propojení a načtení databáze. To je využité u jiných souborů kde se prostě jen pomocí require přiloží soubor a lze editovat databázi.
 * 
 */