from pathlib import Path

def textify():

    word_list = list()

    this_dir = Path(__file__).parent
    path = f"{this_dir}/unsafes.json"

    with open(path, "r", encoding="utf-8") as file:
        for line in file:

            line = line.rstrip()

            if line != "]" and line != "[":
                interim = f'    "{line}",'
                word_list.append(interim)

    with open(path, "w", encoding="utf-8") as file:
        file.write("[\n")

        for line in word_list:
            file.write(f"{line}\n")
        file.write("]")

textify()

"""
Tohle je jenom hodně rychlá importovačně-formátovací funkce, protože nemám excel
+ jsem línej hledat nějakej formátovací program. Nechci se s tím moc dělat, takže 
pro správné zformátování je potřeba, aby všechny hodnoty byly namačkané nalevo
a nebyly tam tedy žádné mezery.
"""