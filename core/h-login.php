<?php
session_start();

require 'config.php';
require_once 'mod/query_db_data.php';
require_once 'mod/validate_user.php';

$stop = false;


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    /*******
     * 
     * Základ, není ani funkce. Jediný cíl: před načtením sebrat jakákoli data, vyhodnotit je a předat k náslendému zpracování
     * Z důvodu minimalizace a přehlednosti jsou komponenty a funkce často přidány pomocí require
     * U každé vlastní funkce přidané z "mod" je poznámka o jménu souboru
     * 
     * *******/

    $data = [];

    foreach ($_POST as $unsafe_key => $unsafe_data) {

        $key = filter_var($unsafe_key, FILTER_UNSAFE_RAW);
        $val = filter_var($unsafe_data, FILTER_UNSAFE_RAW);

        $data[htmlspecialchars($key)] = htmlspecialchars($val);

    }

    $data['email'] = filter_var(htmlspecialchars($data['email']), FILTER_SANITIZE_EMAIL);

    if (!filter_var(htmlspecialchars($data['email']), FILTER_VALIDATE_EMAIL) === false) {

        if(prepare_data_from_DB($stop, $db, $data)) { //query_db_data.php

            validate_user(...$GLOBALS['data']); //validate_user.php

        }

        else {

            $stop = true;
            echo 'Error-4';

        }

    } else {

        $stop = true;
        echo 'Error-1';

    }

}