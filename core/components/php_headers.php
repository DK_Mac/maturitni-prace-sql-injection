<?php
session_start();

// Standardizovaný php header.
// Všechny důležité requiry. Je komponentou všech článků

require $webroot . '/core/mod/store_newsletter-unsafe.php';  // Funkce, která skladuje odebrané maily do newsletteru
require $webroot . '/core/mod/write_message.php';  // Funkce, která se stará o uložení zpráv

?>

<!DOCTYPE html>
<html lang="cs">

<head>
    <?php
    require $webroot . '/src/html/header.html';  // HTML hlavička. Meta, author a podobné věci
    // Proměnná title je definovaná vždy v řídící komponentě.
    ?>
    <title><?=$title?></title>
</head>

<body class="l-body l-body--unlimit-height">
