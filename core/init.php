<?php

require 'Config.php';

$db->exec("CREATE TABLE IF NOT EXISTS users (
    id INT(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(63) UNIQUE,
    password VARCHAR(128),
    login VARCHAR(64)
)");

$db->exec("CREATE TABLE IF NOT EXISTS comments (
    id int(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    text_message VARCHAR(256),
    timestamp VARCHAR(64)
)");

$db->exec("CREATE TABLE IF NOT EXISTS subscriptions (
        id int(8) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        email VARCHAR (63) UNIQUE
)");

// $statement = $db->prepare("INSERT INTO users (email, password, login) VALUES (:email, :password, :login)");
// $statement -> bindValue(":email", "kyndl@animato.cz", PDO::PARAM_STR);
// $statement -> bindValue(":password", password_hash("i9xHToBY!Bcq2*so6Mxf", 
// PASSWORD_DEFAULT));
// $statement -> bindValue(":login", "never", PDO::PARAM_STR);
// $statement->execute();


// $db = null;

// Toto je initový soubor, není nijak propojen se zbytkem světa, pouze slouží jako taková konzole pro přidávání údajů.