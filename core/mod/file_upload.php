<?php
session_start();

$target_dir = $_SERVER['DOCUMENT_ROOT'] . '/core/source/files/';

$target_file = $target_dir . basename($_FILES["upload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


// Zkontrolovat, jestli file_exists
if (file_exists($target_file)) {

    $target_file = $target_dir .' | '. date("y-m-d-H-m-s").' | '. basename($_FILES["upload"]["name"]);

}

// Povolit jedině CSV. Respektive zakázat ostatní
if ($imageFileType != "csv") {

    echo "Omlouváme se, ale zadaný soubor není CSV";
    $uploadOk = 0;

}

if ($uploadOk == 0) {

    header('Location: /Categories/' .htmlspecialchars($_POST['origin']) . '/');
    $_SESSION['curr-action'] = 'FAIL-1';  // Vzhledem k tomu, že tohle je přes jquery, nejde tady prostě zavolat E_2_lines

} else {

    if (move_uploaded_file($_FILES["upload"]["tmp_name"], $target_file)) {  
        // Tady jen té nově vytvořené věci dávám místo

        $_SESSION['curr-action'] = 'SUCCESS';
        header('Location: /Categories/' .htmlspecialchars($_POST['origin']) . '/');

    } else {

        header('Location: /Categories/' .htmlspecialchars($_POST['origin']) . '/');
        $_SESSION['curr-action'] = 'FAIL-2';

    }
}

// Čistý upload, je tady potřeba držet řadu bezpečnostních kritérií typu koncovky, jestli není prázdný a podobně. 
// Součástí matiky a jejích komponent