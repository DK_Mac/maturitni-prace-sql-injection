<?php

$filename = ('../source/files/' . htmlspecialchars($_POST['name']));
$ending = '.csv';

while (file_exists($filename . $ending)) {

    $filename .= '__new';

}

function make_array($data) {
    // Jop, myslel jsem, že to bude delšejší :)

    return preg_split("/\r\n|\n|\r/", $data);

}

$csv = fopen($filename . $ending, 'w');

$header_data=array(htmlspecialchars($_POST['name']));

fputcsv($csv, $header_data);

foreach(make_array(htmlspecialchars($_POST['data'])) as $row) {

    $value = array($row);
    fputcsv($csv, $value);

}

fclose($csv);
echo 'OK';

// Jednoduchý soubor, který vytvoří novou složku a přidá do ní potřebná data.
// Součást matiky a statistiky