<?php

require $webroot . '/core/config.php';


function insert_new_newsletter($db) {

    /*  Sekce, která zjistí, jestli uživatel už neexistuje...  */

    $email = htmlspecialchars($_POST['newsletter']);

    $statement = $db->prepare("SELECT email from subscriptions WHERE email = '$email'");
    $statement->execute();
    // Toto je závadná část kódu, ačkoli to tak na první pohled nemusí vypadat
    // htmlspecialchars lokálně zakóduje speciální znaky, avšak nelze ji použít jako sanitizační fci.
    // Z toho důvodu je toto query náchylné pro útoky.

    foreach($statement as $value) {
        E_2_lines('Zadaný email, tedy ' . $value[0] . ', již je v databázi', 'Zkuste to prosíme znovu, nebo zkontrolujte spelling :)', 'danger');
        $block = True;
    }

    if (!isset($block)) {
        // Bezpečná vkládací část funkce.

        foreach ($_POST as $unsafe_data) {

            $val = filter_var(htmlspecialchars($unsafe_data), FILTER_UNSAFE_RAW);

            $statement = $db->prepare("INSERT INTO subscriptions (email) VALUES (:email)");
            $statement -> bindValue(":email", htmlspecialchars($val), PDO::PARAM_STR);
            $statement -> execute();

            E_2_lines("Úspěšně jste se přihlásili k odběru newsletteru!", "Děkujeme za podporu, první mail Vám přijde již brzy", "success");

        }

    }

}

if (isset($_POST['newsletter'])) {
    insert_new_newsletter($db);
    // Kontrola, jestli byl zadán požadavek na aktualizaci databáze
}
