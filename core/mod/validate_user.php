<?php
function validate_user($usernames, $passwords, $data, $stop, $db) {

    /************
     * 
     * Validate_user:
     * Vezme jméno uživatele, porovná ho s jmény v databázi
     * Když najde match, zapamatuje si id
     * Porovná hesla mezi sebou, v případě úspěchu vyhodí OK, pošle to zpátky uživateli.
     * 
     ***********/

    if ($stop === FALSE) {

        $user_id = (function($usernames, $data) {

            foreach ($usernames as $id => $val) {

                if (htmlspecialchars($data['email']) == $val) {
    
                    $user_id = $id;
                    break;
    
                }
    
            }
    

        })($usernames, $data); // TEST THIS!!!


        if (password_verify(htmlspecialchars($data['password']), $passwords[$user_id])) {

            $_SESSION['user_id'] = $user_id;
            $_SESSION['login_time'] = date_create();

            $statement = $db -> prepare("UPDATE users SET login = :login WHERE email = :email");
            $statement -> bindValue(":login", date_format(date_create(), "d.m.Y"), PDO::PARAM_STR);
            $statement -> bindValue(":email", htmlspecialchars($usernames[$user_id]), PDO::PARAM_STR);
            $statement -> execute();

            echo 'OK';

        }

        else {

            echo 'Error-3';

        }

    }

}