<?php

function insert_new_message($db) {
    if (isset($_POST['text_in']) && htmlspecialchars($_POST['text_in']) != '') {
        $statement = $db -> prepare('INSERT INTO comments (text_message, timestamp) VALUES(:text_message, :timestamp)');
        $statement -> bindValue(':text_message', htmlspecialchars($_POST['text_in']), PDO::PARAM_STR);
        $statement -> bindValue(':timestamp', date('Y-m-d'), PDO::PARAM_STR);
        $statement -> execute();
        E_1_line('Zpráva byla úspěšně odeslána', 'success');
    }
    else {
        E_2_lines('Zpráva nebyla odeslána', 'Zřejmě jste odeslali prázdnou zprávu, zkuste to prosím znovu', 'danger');
    }
}


if(isset($_POST['action']) && htmlspecialchars($_POST['action']) == 'message') {
    insert_new_message($db);
}