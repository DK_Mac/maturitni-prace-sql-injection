<?php
$link = false;

if (isset($_GET['target']) && htmlspecialchars($_GET['target']) != '') {

    $allArr = [];

    $path = $_SERVER['DOCUMENT_ROOT'] . '/core/source/files/';
    $file = fopen($path . htmlspecialchars($_GET['target']), 'r');
    while (($data = fgetcsv($file)) !== false) {

        foreach($data as $row) {

            $allArr[] = $row;

        }

    }

    fclose($file);

    $link = htmlspecialchars($_GET['target']);

}

/* Ověřím, jestli přišla žádost a adekvátně na ní zareaguji

Následně otevřu soubor pro čtení a uložím jeho obsah.

Součást matiky a CSV věcí

*/