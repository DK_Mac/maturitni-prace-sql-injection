<?php

require $root . '/core/config.php';  // Davaj databázi

function query_data($db) {
    // Jednoduchý modul na zeptání se na data

    $statement = $db->prepare("SELECT * FROM users ORDER BY id DESC");
    $statement->execute();
    return $statement;

}

function validate_password($id, $db) {
    $data = query_data($db);

    foreach($data as $row) {

        if ($row['id'] == $id) {

            if (password_verify(htmlspecialchars($_POST['old']), $row['password'])) {

                return True;

            }

            else {

                E_2_lines("Omlouváme se, ale při procesu nastala chyba", "Zadané heslo není správné", "danger");
                return False;

            }

        }

    }

}

function part_of_unsafes($new, $root) {

    // Zjistí, jestli daná fce nepatří na seznam 200 nejběžnějších hesel

    $pswd_file = file_get_contents($root . "/core/source/passwords/unsafes.json");
    $data = json_decode($pswd_file);

    foreach ($data as $item) {

        if ($item == $new) {

            return True;

        }

    }

    return False;

}

function check_new_passwords($root) {
    // Kontroluje nové heslo, jestli je dost bezpečné,...

    $new = htmlspecialchars($_POST['password']);
    // Tohle je zkratka, abych to nemusel pokaždé vypisovat, 
    //vzhledem k tomu, že s tím budu ještě dál pracovat...

    if ($new == htmlspecialchars($_POST['check'])) {

        if (strlen($new) < 6) return False;

        if (part_of_unsafes($new, $root)) return False;

    }

    else return False;

    return True;

}

function update_password($db, $user) {

    // Samotná Update fce do databáze. Aby to sem došlo, muselo to projít docela dost kontrolami...

    $data = query_data($db);
    foreach ($data as $row) {

        if ($row['id'] == $user) {

            $statement = $db -> prepare("UPDATE users SET password = :password WHERE id = :id");
            $statement -> bindValue(":password", password_hash(htmlspecialchars($_POST['password']), PASSWORD_DEFAULT));
            $statement -> bindValue(":id", $user, PDO::PARAM_INT);
            $statement -> execute();

            E_2_lines("Změna hesla proběhla úspěšně", "Heslo bylo aktualizováno", "success");

        }

    }

}


function change_psw($db, $root) {

    // První, úvodní fce, která proběhne na zavolání. De facto jen volá další.

    if (isset($_POST['old']) && isset($_POST['password']) && isset($_POST['check'])) {

        $user = htmlspecialchars($_SESSION['user_id']); // člověk si nikdy není dost jistý

        // $conditions = validate_password($user, $db) ? check_new_passwords($root) : False;

        if (validate_password($user, $db)) {

            if (check_new_passwords($root)) {

                update_password($db, $user);

            } else E_2_lines("Nastala chyba při ověřování", "Zadané heslo není dostatečně bezpečné. Zkontrolujte prosím, zda splňuje všechny podmínky", "danger");

        }

        else E_2_lines("Nastala chyba při ověřování", "Zadali jste špatné heslo", "danger");
    }

}

change_psw($db, $root);