<?php
require $webroot . '/core/config.php';  // Davaj databázi

function query_data($db) {
    // Jednoduchý modul na zeptání se na data

    $statement = $db->prepare("SELECT * FROM users ORDER BY id DESC");
    $statement->execute();
    return $statement;

}

function check_if_get() {
    // Kontrola, jeslti mám GET. Aby mi to zbytečně neházelo chyby

    if (isset($_GET['action'])) {
        return True;
    }
    else {
        return False;
    }

}

function error_if_exists($entry, $db) {
    // Další utile-funkce, zjišťuje, jestli zadaný user existuje

    $data = query_data($db);
    foreach($data as $row) {

        if($row['email'] == $entry) {

            return True;  // Pokud tady něco najdu, tak to zakážu

        }
        else {

            $block = False;

        }

    }

    if (!$block) {

        return False;

    }

}

function add($db) {

    // Funkce, která vytváří nový sample uživatelský profil...

    if(!error_if_exists("example@example.org", $db)) {
        // Pokud neexistuje, tak...

        $statement = $db->prepare("INSERT INTO users (email, password, login) VALUES (:email, :password, :login)");
        $statement -> bindValue(":email", "example@example.org", PDO::PARAM_STR);
        $statement -> bindValue(":password", password_hash("123456", PASSWORD_DEFAULT));
        $statement -> bindValue(":login", "never", PDO::PARAM_STR);
        $statement->execute();

        E_2_lines("Akce proběhla úspěšně", "Uživatel úspěšně přidán do databáze", "success");
        // Tahle fce se tady bude objevovat docela často, tiskne error hlášku (soubor popup_functions.php)

    }

    else {

        E_2_lines("Nastala chyba při provádění akce", "Již existuje profil nového uživatele", "danger");

    }

}

function update($db) {

    // Jak jméno napovídá, tohle se stane, když uživatel zmáčkne tlačítko update...

    if(!error_if_exists(htmlspecialchars($_GET['email']), $db)) {

        $data = query_data($db);
        foreach($data as $row) {

            if($row["id"] == htmlspecialchars($_GET['id'])) {

                $new_mail = $_GET['email']; // Tohle je zase nebezpečné, vstup není ošetřený.
                $new_id = $_GET['id'];

                $statement = $db->prepare("UPDATE users SET email = '$new_mail' WHERE id = '$new_id'");
                $statement -> execute();

                E_2_lines("Akce proběhla úspěšně", "Data byla úspěšně uložena", "success");

            }

        }

        // SQLI, které zneužívá tuhle díru, tak je například něco takovéhleho...
        // XYZ', email = password -- -
        // Výsledek pak vypadá nějak takhle...
        // $statement = $db -> prepare("UPDATE users SET email = 'XYZ', email = password -- WHERE id = 1");
        // Tohle prohodí do sloupečku emails hodí hodnoty passwords, ty se pak zobrazí v nabídce...

    }
    else {
        E_2_lines("Nastala chyba při provádění akce", "Tento email již je v databázi", "danger");
    }

}


function delete($db) {

    // Tohle asi není moc potřeba komentovat...

    $statement = $db->prepare("DELETE FROM users WHERE id = :id");
    $statement->bindValue(":id", htmlspecialchars($_GET['id']), PDO::PARAM_INT);
    $statement->execute();

    E_2_lines("Akce proběhla úspěšně", "Data byla úspěšně zničena", "success");

}


function process($db) {
    // Tady není moc co zkoumat. Centrální fce, zkontroluje, jestli se něco po ní chce, pokud se po ní něco chce, tak to zavolá
    if(check_if_get()) {

        if (htmlspecialchars($_GET['action']) == "add") {
            add($db);
        }

        if (htmlspecialchars($_GET['action']) == "update") {
            update($db);
        }

        if (htmlspecialchars($_GET['action']) == "delete") {
            delete($db);
        }
    }

}

process($db);
