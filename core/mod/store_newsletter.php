<?php

require $webroot . '/core/config.php';


function check_if_exists($val, $db) {

    // Utile funkce, jenom zjišťuje, jestli tady náhodou neexistuje duplikát

    $statement = $db->prepare("SELECT * FROM subscriptions WHERE email = :email");
    $statement -> bindValue(":email", htmlspecialchars($val), PDO::PARAM_STR);
    $statement -> execute();

    if ($statement -> fetch(PDO::FETCH_ASSOC)) return False;
    else return True;

}


if (isset($_POST['newsletter']) && htmlspecialchars($_POST['newsletter']) != '') {

    // De facto jen insert into DB s trochou nadbytečný sanitizace...

    foreach ($_POST as $unsafe_key => $unsafe_data) {

        $val = filter_var(htmlspecialchars($unsafe_data), FILTER_UNSAFE_RAW);

        if (check_if_exists($val, $db)) {

            $statement = $db->prepare("INSERT INTO subscriptions (email) VALUES (:email)");
            $statement -> bindValue(":email", htmlspecialchars($val), PDO::PARAM_STR);
            $statement -> execute();

            E_2_lines("Úspěšně jste se přihlásili k odběru newsletteru!", "Děkujeme za podporu, první mail Vám přijde již brzy", "success");

        }

        else {

            E_2_lines("Nastala chyba...", "Na tento email již newslettery pravidelně odcházejí", "danger");

        }

        break;

    }

}
