<?php

session_start();

function eval_get() {

    // Funkce, která zničí sessionu, pokud se uživatel odhlásí. Efektivně mu to znemožní cokoli adminího dělat
    // Pro zakázání přístupu je fce auto_logger()

    if (isset($_GET['symbol']) && htmlspecialchars($_GET['symbol']) == 'logout') {

        session_destroy();

        header("Location: /");

    }

}

eval_get();