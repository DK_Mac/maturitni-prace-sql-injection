from pathlib import Path


def get_root()->Path:
    return Path(__file__).parent.parent.parent
    # Docela sebedekriptivní název...


def eval_file(root:Path, path:str, source:str)->str:
    """
    Univerzální fce, zjistí, jestli je aktuální verze safe, nebo unsafe.
    První argument je root, to je univerzální a povinná věc, zjišťuje document root.
    Druhý argument je path, čili cesta k souboru
    Třetí argument je konkrétní řádka, kterou kontroluji.
    """
    with open(f"{root}/{path}", "r", encoding="utf-8") as file:
        for line in file:
            if source in line:
                return 'Safe'
        return 'Unsafe'


def sqli_status(PY = False):
    """
        Většinově printovací funkce.
        ... De facto jen udělá tabulku s tím, co jak je.
        Může být zavolána buď z command line (PY = False, vrací "Souhrn dokončen")
        Nebo z sqli funkce, pak vrací status.
    """
    print("\n Zjišťuji aktuální stav souborů...\n")

    root = get_root()

    status = {
        "Index SQLI" : eval_file(root, "index.php", 'url: "/core/h-login.php",'),
        "Newsletter SQLI" : eval_file(root, "core/components/php_headers.php", "require $webroot . '/core/mod/store_newsletter.php'"),
        "Users SQLI" : eval_file(root, 'admin/controller/index.php', "require $webroot . '/core/mod/inserter_users.php")
    }

    print("\tID:\t\tPopis hrozby:\t\tStav:")
    print("---"*23)
    num = 0
    for key, val in status.items():
        print(f"\t{num}\t\t{key}\t\t{val}")
        num += 1

    print("")


    if PY:
        return status
    else:
        return "Souhrn ukončen"


def change_target_content(path:str, state:str, target:str)->bool:
    """
        Tahle funkce si nejdřív zjistí, co se po ní chce (if else)
        Potom otevře a zkopíruje obsah daného souboru. Tam změní jednu danou řádku
        Poslední with open je pak vepsání změn do souboru.
    """

    if state == "Safe":
        find = f"{target}"
        replace = f"{target}-unsafe"
    else:
        find = f"{target}-unsafe"
        replace = f"{target}"

    with open(path, "r", encoding="utf-8") as file:
        replacement = ""
        for line in file:
            line = line.rstrip()
            changes = line.replace(find, replace)
            replacement = replacement + changes + "\n"

    with open(path, "w", encoding="utf-8") as fout:
        fout.write(replacement)
        return True


def update_sqli(target:str, status)->bool:
    """
        Tady se zjistí, co chce uživatel změnit, zkontroluje se validita a rozešlou se další příkazy.
        1. část: ověřování inputů, jestli to jde provést.
        2. část: konstrukce cesty k cílovému souboru
        3. část: zavolání funkce na změny
    """
    try:
        int(target)
    except:
        print(f"\n\tZadali jste neplatný znak {target}. Požadavek zamítnut")
        return False

    i = 0
    code, state = "", ""  # Snaha vysvětlit VSCode, že code a state nebudou nikdy unbound
    for key, val in status.items():
        if (i == int(target)):
            code, state = key, val
            break
        i += 1
        if i >= len(status):
            print(f"\n\tChyba, kód {target} není přidělen ničemu. Požadavek nebyl proveden")
            return False

    # Zkonstruovat cestu

    code = code.replace(" SQLI", "")
    code = code.lower()

    if (code == "index"):
        path = f"{get_root()}/index.php"
        target = "h-login"

    elif (code == "newsletter"):
        path = f"{get_root()}/core/components/php_headers.php"
        target = "store_newsletter"

    elif (code == "users"):
        path = f"{get_root()}/admin/controller/index.php"
        target = "inserter_users"

    else: 
        print("\n\tNastala chyba, soubor ke změně nebyl nalezen...")
        return False

    change_target_content(path, state, target)

    return True



def sqli()->str:
    """
        Ta hlavní fce, kterou volám. V podstatě jen rozcestník.
        ALE! Je tam rekurze. Ta spočívá v tom, že se nám to bude objevovat znovu a znovu, dokud to neukončíme.
        Jsou tady i podmínky na vyskočení z rekurze, atd
    """
    def rec_query_sqli():

        status = sqli_status(True)

        print("-- q pro ukončení, zadejte číslo indexu --")
        target = input("Zadejte index složky ke změně: ")

        if target != "q" and target != "quit" and target != "exit":
            res = update_sqli(target, status)

            if res == True: 
                print(f"\n\tIndex {target} úspěšně změněn")

            rec_query_sqli()

    rec_query_sqli()

    return "\n--Rozhraní ukončeno--\n"
