# -*- coding: utf-8 -*-
import os
from os.path import exists


def htaccess():

    if exists('.htaccess'):
        os.rename(r'.htaccess', r'.htaccess-disabled')
        return('\n\n.htaccess security turned OFF\n\n')

    else:
        os.rename(r'.htaccess-disabled', r'.htaccess')
        return('\n\n.htaccess security turned ON\n\n')

'''

Program, který by na reálném serveru neměl co dělat. 
    Tady vytváří takovou zkratku pro vypínání a zapínání globálního .htaccessu
    Ten zajišťuje bezpečnost, mimo jiné skrz striktní CSP Policy.

'''