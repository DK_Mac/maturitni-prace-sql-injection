# -*- coding: utf-8 -*-
import os
from pathlib import Path


def access_folder()->str:
    root = Path(__file__).parent.parent.parent
    dir = os.path.join(root, 'src/css')
    return dir

    # Nejdřív zjisít dir a pak najde složku css


def find_index(dir:list):
    for file in dir:
        if file == "main.scss" or file == "_index.scss":
            return file
    # Univerzální funkce, která prohledá vstupní dir a následně vrátí hlavní soubory


def check_character(path:str, file:str):
    """
    Tahle funkce kontroluje charakter hledané věci
    Pokud to JE nový index v nové složce, reportuju ho
    Pokud to NENÍ nic extra zajímavého, jedu dál
    """
    try:
        with open ((path + "/" + file), "r", encoding="utf-8") as test:
            for line in test:
                pass  # Test toho, jestli to jde...
        return file
    except:
        return False


def check_if_dir(path:str):
    # Funkce, která kontroluje, jestli je vstupní věc directory
    try:
        return os.listdir(path)
    except:
        return None


def check_main(path:str, main:str)->None:
    # Tahle funkce dostává celkovou path a jméno mainu
    # Z toho zkontroluje, které přidružené věci jsou typu dir
    # Z těch dirů najde index
    # Indexy přidá do main.scss
    main = path + main
    avail_poss = check_if_dir(path)
    if avail_poss:
        avail_dirs = list()
        for poss in avail_poss:
            if check_if_dir(f"{path}/{poss}"):
                avail_dirs.append(poss)
        with open(main, "w", encoding="utf-8") as file:
            for dir in avail_dirs:
                file.write(f"@import './{dir}/index';\n")


def check_minions(path:str, dirs:list)->None:
    # Dělá de facto to samé jako check_main, ale malinko komplexněji
    # Komplexněji proto, že se celkově jedná o komplecnější situaci
    # Soubory se mohou jmenovat jinak a celkově to teoreticky může být trošku divočejší
    for dir in dirs:
        test = path + "/" + str(dir)
        if(check_if_dir(test)):
            index = find_index(os.listdir(test))
            folder = check_if_dir(test)
            if folder:
                for element in folder:
                    src = check_if_dir(f"{path}/{dir}/{element}")
                    if index and src:
                        with open(f"{test}/{index}", "w", encoding="utf-8") as file:
                            for one in src:
                                one = one.replace("_", "")
                                one = one.replace(".scss", "")
                                fin = f"@import './src/{one}';\n"
                                file.write(fin)

def style():

    print("Probíhá aktualizace indexů...")

    files = os.listdir(access_folder()) # Najdi webroot a složku s CSS, vypiš obsah
    main = f"/{str(find_index(files))}"  # Najdi main.scss
    check_main(access_folder(), main)   # Zkontroluj a aktualizuj Main.scss
    check_minions(access_folder(), files)

    return "Aktualizace indexů proběhla úspěšně"
