# -*- coding: utf-8 -*-

def help():
    help_text = """\n---Nastavení souboru---\n
        Základ: python3 config.py
        \n--Funkce--\n
        --help\t\tZobrazí nápovědu

        --htaccess\tVypne / Zapne centrální htaccess

        --status\tZjistí a zobrazí stav zabezpečení
        --sqli\t\tSpustí interakci pro změnu stavu zabezpečení

        --style\t\tNajde a nastaví nové scss soubory

    """

    return(help_text)