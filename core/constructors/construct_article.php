<?/*
--- Constructor článku ---
Možná. Tohle je součást vkladače článku. Tahle věc dá vlastně dohromady všechny komponenty, co jsou potřeba k udělání stránky.
Bavíme se o sub-řídícím souboru
*/?>

<?php
require $webroot . '/core/mod/popup_functions.php'; // Errory a podobné věci --> musí být nahoře, jsou používány...
?>

<?php
require $webroot . '/core/components/php_headers.php';  // PHP hlavička
?>

<?php 
require $webroot . '/src/html/navbar.html';  // Ten vrchní navbar
?>

<?php 
require $webroot . '/src/html/Articles/' . $title . '.html';  // Samotné tělo článku. Article definováno na řídící stránce
?>

<?php
require $webroot . '/src/html/footer.html';  // Patička s unsafe věcmi
?>

<?php
require $webroot . '/src/html/nav.html';  // Hlavní navigace
?>

<?php
require $webroot . '/src/html/scripts.html';  // Skripty
?>

</body>

</html>

<?/*
Na tyhle dva koncové tagy nahoře jinde nezbylo místo, tak jsou tady.
Ale jinak takhle by se daly udělat všechny stránky, kdyby tady bylo všechno vytvořené přes CMS,
ne přes tuhle "semi-md" strukturu
*/?>