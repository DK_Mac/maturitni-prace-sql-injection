<?php
session_start();

require 'config.php';

if (isset($_POST) && htmlspecialchars($_POST['email']) !== "" && htmlspecialchars($_POST['password']) !== "") {
    // Obecná podmínka, jestli umím, nebo ne... :)


    $hash = function($email, $db) {
        /**
         * Anonymní fce, na zkontrolování hesla. Bude použita později.
         * Tahle část je bezpečná, alespoň pokud vím. Importuje sanitizovanou verzi emailu
         * Sama si sanitizuje password. Používá prepared statement
         * Zjistí, zkontroluje, vrátí výsledek 
         **/

        $statement = $db->prepare("SELECT password FROM users WHERE email = :email");
        $statement->bindValue(':email', $email, PDO::PARAM_STR);
        $statement->execute();

        $block = TRUE;
        foreach($statement as $row) {

            $block = FALSE;

            if (password_verify(htmlspecialchars($_POST['password']), $row['password'])) {
                return 'correct';
            }
            else return 'incorrect';

        }

        if ($block) {

            return 'incorrect';

        }

    };

    // Tady jsou připravené komponenty, které budu používat...
    // Pro přehlednost jsou vypsané tady, ten kód by jinak vypadal jak nekonečná špageta...
    // Asi mám chuť na špagety.

    $email = $_POST['email'];
    $c_hash = $hash(htmlspecialchars($_POST['email']), $db);
    $query = "SELECT * FROM users WHERE email = '$email' AND 'correct' = '$c_hash'";
    // $query --> problém. 1x jsem zapomněl na zhashování emailu. Jedinkrát. To je ten problém.

    $statement = $db->query($query);

    if ($statement->fetch(PDO::FETCH_ASSOC)) {
        // Tohle se pak už jen veze... Počítám s tím, že se jeden uživatel vytiskne...
        // Což ale pořád funguje. Sice to SQL Injection provede pro všechny, ale to je jedno. 
        // No a session taky ničemu nezabrání, protože jako constraint je nastavené, že musí být definovaná a nesmí být prázdná...
        // $email doopravdy něco je...
        // A vzhledem k tomu, že se prostě nenajde něco, co by splňovalo WHERE, tak se ani nic neaktualizuje.
        // Útok za sebou nezanechá stopu.

        $_SESSION['user_id'] = $email;
        $_SESSION['login_time'] = date_create();

        $statement = $db -> prepare("UPDATE users SET login = :login WHERE email = :email");
        $statement -> bindValue(":login", date_format(date_create(), "d.m.Y"), PDO::PARAM_STR);
        $statement -> bindValue(":email", $email, PDO::PARAM_STR);
        $statement -> execute();

        echo 'OK';

    }
    else {

        echo ("Error-1");

    }

}

// Řešení: ' OR 1=1-- 
