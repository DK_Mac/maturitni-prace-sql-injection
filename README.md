# Maturitní práce - SQL injection

Maturitní práce z předmětu IKT, zadané téma je rozbor a replikace SQL Injection, útoku na databázi.

Testovací rozhraní prostřednictvím MAMP

vedoucí práce: Martin Sourada

# Autor
Daniel Kyndl, 6S

# Uživatelská příručka a komentáře kódu
1. Statické front-end věci jsou pouze v adresáři src
2. back-end php je v adresáři core
3. Modul statistické matiky je v adresáři categories/Math/
4. Index je login form
5. Admin jsou věci v zabezpečené sekci
6. Docs obsahuje manuál k dostání se do newsletteru.
7. Categories jsou přístupné stránky

# Pokyny ke spuštění
1. alternativa: Vývojový program
   > Osobně vyvíjím projekt prostřednictvím aplikace MAMP, konkrétní nastavení je Apache server, PHP8, mySQL databáze prostřednictvám phpMyAdmin. 
   > Alternativy k MAMPu zahrnují například XAMPP, nebo WAMP
   - Pokyny ke spuštění: 
     - Rozbalit kód do složky htdocs (popřípadě ekvivalentu)
     - ze složky src/db naimportovat do phpmyadmin aplikace databázi
     - Spustit
2. alternativa: Manuální spuštění
   - Potřebné programovací jazyky, instalace
     - Nainstalovaný a vhodně nastavený Docker NEBO:
     - Setup vlastního apache serveru
     - Nainstalovat PHP8, nebo vyšší
     - Nainstalovat mySQL v nejnovější verzi
     - Nainstalovat Adminer / PhpMyAdmin
     - Nainstalovat program na přístup k serveru pomocí FTP protokolu, například Filezilla
   - Postup instalace:
     - Nahrát soubory do odpovídající lokace na serveru, zapnout a zprovoznit
     - Nahrát databázi do databázové aplikace
3. Dodatečné programovací jazyky
   - Jsou využity, ale nejsou aktivně nutné k běhu aplikace
     - Python 3.x.x
     - SCSS (preprocesor, použitelný například jako rozšíření VSCode)

# Config aplikace
- Rozhraní obsahuje i z příkazové řádky obsluhovaný config.
- Ten slouží primárně pro vývojové účely, lze přes něj ale i zapínat / vypínat jednotlivé bezpečnostní chyby.
- python3 config.py --help vypíše všechny dostupné příkazy
- Zdrojový kód ke config.py lze nalézt v core/config/ adresáři.
