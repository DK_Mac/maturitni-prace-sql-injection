<?php
	session_start();
	require $_SERVER['DOCUMENT_ROOT'] . '/core/mod/skip_login.php';

?>

<!DOCTYPE html>
<html lang="cs">
<head>

	<?php
	require $_SERVER['DOCUMENT_ROOT'] . '/src/html/header.html';
	?>

	<title>Login</title>
</head>

<body class="l-body l-body--center">

<div class="o-login">
	<div class="o-login__talkie">
		<h2 class="o-login__headline">
			Bezpečný login
		</h2>
		<p class="o-login__text">

			Bezpečná a plně ošetřená forma loginu, která neselže a není nijak zneužitelná.

		</p>
		<hr>
		<p class="o-login__text">

			email: jan.novak@email.cz<br>
			heslo: 123456

		</p>
	</div>
	<div class="o-login__form-holder">

		<form class="e-form" method="post" id="login">

			<label class="e-form__label" for="email">Zadejte Váš email</label>
			<input class="e-form__input" type="email" name="email" id="email" autocomplete="true">

			<label class="e-form__label" for="password">Zadejte Vaše heslo</label>
			<input class="e-form__input" type="password" name="password" id="password" autocomplete="true">

			<button type="submit" class="c-button c-button--submit">
				Ověřit
			</button>

		</form>

	</div>

</div>

<div class="e-cheats e-cheats--offset-l">
        <h4>Cheat sheet</h4>
        <ul>
            <li>' OR 1=1-- -</li>
        </ul>
    </div>

<?php
require $_SERVER['DOCUMENT_ROOT'] . '/src/html/nav.html';
?>



<?php
	require $_SERVER['DOCUMENT_ROOT'] . '/src/html/scripts.html';
?>
<script>
	// Tohle je funkce, která sebere data po submitnutí, následně je odešle do loginu pro zpracování.
	$(document).ready(function() {
        $("#login").submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: "/core/h-login-unsafe.php",
                type: "post",
                data: {
                    email: $("#email").val(),
                    password: $("#password").val()
                },
                success: function(response) {
                    if (response === "OK") {
						window.location.href="/admin/"
                    } else {
                        alert((() => {
								switch(response) {
									case "Error-1":
										return("Chyba, zadané jméno nebo heslo neexistují");
										break;
									case "Error-2":
										return("Omlouváme se, ale nastala chyba na straně serveru");
										break;
									case "Error-3":
										return("Omlouváme se, ale zadané jméno nebo heslo nejsou správně");
										break;
									default:
										return(response);
								}
							})()
						);
                    }
                }
            })
        })
    })
</script>


</body>
</html>
