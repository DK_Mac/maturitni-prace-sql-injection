# -*- coding: utf-8 -*-
import sys
from core.config.help import help
from core.config.security import htaccess  # Chci to rozdělit na htaccess...
from core.config.autostyle import style
from core.config.login_sqli import sqli, sqli_status  # A MP-related věci

def do_work():
    """ Tahle funkce procesuje input z command line, přidává podporu pro neat spuštění 
        POZOR! Funguje jen na verzi vyšší než 3.0, např. Mac má defaultní commandline python 2.7
        Je potřeba použít něco jako python3 na začátku
    """
    args = sys.argv
    args = args[1:] # První element argumentů je config.py

    if len(args) == 0:
        print('Zadejte nějaký příkaz...')
    else:
        for a in args:
            if a == '--help':
                print(help())
            elif a == '--htaccess':
                print(htaccess())
            elif a == '--style':
                print(style())
            elif a == '--sqli':
                print(sqli())
            elif a == '--status':
                print(sqli_status())
            else:
                print(f"ERROR: Funkce config.py nepodporuje symbol {a}")

if __name__ == '__main__':
    do_work()
