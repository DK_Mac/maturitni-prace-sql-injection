<?php
	session_start();

	$root = $_SERVER['DOCUMENT_ROOT'];

    require $root . '/core/security/auto-logger.php';
	require $root . '/core/mod/popup_functions.php';
	require $root . '/core/mod/pass_change.php';

	?>

<!DOCTYPE html>
<html lang="cs">
<head>

	<?php
		require $root . '/src/html/header.html';
	?>

	<title>Změna hesla</title>
</head>

<body class="l-body l-body--center">

<div class="o-login">
	<div class="o-login__talkie">
		<h2 class="o-login__headline">
			Změna hesla
		</h2>
		<p class="o-login__text">
			Zde si můžete bezpečně změnit heslo
		</p>
		<hr>
		<p class="o-login__text">
			Heslo musí být minimálně 6 znaků dlouhé a musí být dostatečně bezpečné. <br>
			Bezpečnost se ověřuje porovnáním hesla s databází 200 nejpoužívanějších (a nejhorších) hesel.
		</p>
	</div>
	<div class="o-login__form-holder">

		<form class="e-form e-form--mb-0" action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>" method="post" id="login">

			<label class="e-form__label" for="old">Zadejte staré heslo</label>
			<input class="e-form__input" type="password" name="old" id="old" autocomplete="true">

			<label class="e-form__label" for="password">Zadejte nové heslo</label>
			<input class="e-form__input" type="password" name="password" id="password" autocomplete="false">

			<label class="e-form__label" for="check">Nové heslo podruhé</label>
			<input class="e-form__input" type="password" name="check" id="check" autocomplete="false">

			<button type="submit" class="c-button c-button--submit">
				Změnit
			</button>

		</form>

	</div>
</div>

<?php
	require $root . '/src/html/nav.html';
?>



<?php
	require $root . '/src/html/scripts.html';
?>

</body>
</html>