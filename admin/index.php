<?php

	session_start();
	require '../core/security/auto-logger.php';
	$webroot = $_SERVER['DOCUMENT_ROOT'];

?>


<!DOCTYPE html>
<html lang="cs">
<head>
	<?php
	require $webroot . '/src/html/header.html';
	$title = "Stránka administrace"
	?>

	<title>
		<?=$title?>
	</title>
</head>
<body class="l-body l-body--unlimit-height">


<?php
require $webroot . '/src/html/navbar.html';  // Navbar společný s články. Stejý princip...

/*
Tuhle věc nechci udělat do article formy.
Proč?
--> Nechci mít obsah tohohle volně v HTML souboru v src.
--> Z principu věci. Na reálném serveru na produkci by tahle stránka byla docela dost propojená s CMS,
	bylo by tady hodně PHP. Jak v grafech, tak v úpravách obsahu a podobně...

--> Ano, asi by mi to ušetřilo práci a obhájil bych si to, ale chci tuhle stránku udělat jakoby co nejbližší realitě
+ Tak aby byla co nejsnáze použitelná pro reálnou aplikaci (samozřejmě s kompletním přestylováním)
*/
?>


<div class="anchor" id="nav-1"></div>
<section class="l-section">
	<div class="l-section__bg l-section__bg--chart"></div>
	<h1 class="c-headline c-headline--no-mb">Denní statistika</h1>
	<p class="c-text c-text--mb">/Samozřejmě smyšlená/</p>
	<div class="l-section__chart">
		<canvas id="Stats" width="100" height="70"></canvas>
	</div>
</section>

<div class="anchor" id="nav-2"></div>
<section class="l-section">
<div class="l-section__bg l-section__bg--CMS"></div>

<h1 class="c-headline">CMS</h1>
<p><i>Za poslední týden proběhlo <strong>5</strong> editací obsahu a bylo napsáno <strong>9</strong> nových příspěvků</i></p>
<a href="#" class="c-button c-button--delink c-button--primary c-button--m-16">Přejít do editačního systému</a>
<p><i>--Opět čistě smyšlená věc pro efekt--</i></p>
</section>

<div class="anchor" id="nav-3"></div>
<section class="l-section">
<div class="l-section__bg l-section__bg--admin"></div>

	<h1 class="c-headline">Správa uživatelů</h1>
	<p class="c-text c-text--mb c-text--px">Umožňuje měnit, spravovat a přidávat a odebírat členy</p>

	<a href="/admin/controller/" class="c-button c-button--delink c-button--primary">Přejít na správu uživatelů</a>

</section>

<div class="anchor" id="nav-4"></div>
<section class="l-section">
<div class="l-section__bg l-section__bg--key"></div>

	<h1 class="c-headline">Změna hesla</h1>
	<p class="c-text c-text--mb c-text--px">Umožňuje změnu hesla</p>

	<a href="/admin/password_change/" class="c-button c-button--delink c-button--primary">Přejít na změnu hesla</a>

</section>


<?php
require $webroot . '/src/html/nav.html';
?>
<?php
require $webroot . '/src/html/scripts.html';
?>
</body>
</html>