<?php

	session_start();

    $webroot = $_SERVER['DOCUMENT_ROOT'];
	require $webroot . '/core/security/auto-logger.php';  // Automaticky přihlaš / Odhlaš a podobně
    require $webroot . '/core/mod/popup_functions.php';  // Chybové hlášky
    require $webroot . '/core/mod/inserter_users-unsafe.php';  // Tiskne uživatele

?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <?php
        require $webroot . '/src/html/header.html';  // Univerzální header
    ?>
    <title>Správa uživatelů</title>
</head>
<body class="l-body l-body--h-center">

    <div class="l-header">
        <h2 class="l-header__h2">Správa uživatelů</h2>
    </div>

    <div class="e-table__wrapper">
        <table class="e-table e-table--bigger">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Poslední přihlášení</th>
                    <th>Akce</th>
                    <form class="e-table__centerer" action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>" method="get" title="Přidat uživatele">
                        <th>
                            <input type="hidden" name="action" value="add">
                            <button type="submit" class="c-button c-button--intable c-button--success">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-person-plus" viewBox="0 0 16 16">
                                    <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                                    <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                                </svg>
                            </button>
                        </th>
                    </form>
                </tr>
            </thead>
            <tbody>
                <?php

                    foreach(query_data($db) as $row) {
                        // Vytiskni všechny uživatele --> pro každého udělej...
                        ?>

                        <tr>
                            <form class="e-table__centerer" action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>" method="get" title="Aktualizovat údaje">
                                <td id="<?=$row['id']?>">
                                    <input class="e-table__input" type="email" name="email" id="email" value="<?=$row['email']?>">
                                </td>
                                <td><?=$row['login']?></td>
                                <td>
                                        <input type="hidden" name="id" value="<?=$row['id']?>">
                                        <input type="hidden" name="action" value="update">
                                        <button type="submit" class="c-button c-button--intable c-button--primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-arrow-repeat" viewBox="0 0 16 16">
                                                <path d="M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 1 .192-.41zm-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9z"/>
                                                <path fill-rule="evenodd" d="M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 7H12.9A5.002 5.002 0 0 0 8 3zM3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9H3.1z"/>
                                            </svg>
                                        </button>
                                </td>
                            </form>
                            <form class="e-table__centerer" action="<?=htmlspecialchars($_SERVER['PHP_SELF'])?>" method="get" title="Odebrat uživatele">
                                <td>
                                    <input type="hidden" name="id" value="<?=$row['id']?>">
                                    <input type="hidden" name="action" value="delete">
                                    <button type="submit" class="c-button c-button--intable c-button--danger">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-person-dash" viewBox="0 0 16 16">
                                            <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                                            <path fill-rule="evenodd" d="M11 7.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z"/>
                                        </svg>
                                    </button>
                                </td>
                            </form>
                        </tr>

                        <?php
                    }

                ?>
            </tbody>
        </table>
    </div>

    <div class="e-cheats">
        <h4>Cheat sheet</h4>
        <ul>
            <li>XYZ', email = password -- -</li>
        </ul>
    </div>

    <?php
        require $webroot . '/src/html/nav.html';  // Standartní lištička
    ?>

    <?php
	require $webroot . '/src/html/scripts.html';  // Skripty
	?>
</body>
</html>
